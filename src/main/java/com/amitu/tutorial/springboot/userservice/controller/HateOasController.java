package com.amitu.tutorial.springboot.userservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amitu.tutorial.springboot.userservice.exception.UserNotFoundException;
import com.amitu.tutorial.springboot.userservice.model.User;
import com.amitu.tutorial.springboot.userservice.repository.UserRepository;

@RestController
@RequestMapping("v2")
public class HateOasController {
	
	@Autowired
	private UserRepository repository;
	
	@GetMapping("/user/{id}")
	public EntityModel<Optional<User>> getUser(@PathVariable("id")Long id) throws UserNotFoundException {
		Optional<User> user = repository.findById(id);
		
		EntityModel<Optional<User>> entityModel = EntityModel.of(user);
		
		Link selfLink = WebMvcLinkBuilder.linkTo(HateOasController.class)
				.slash(user.get().getId())
				.withSelfRel();
		
		Link delLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.
				methodOn(UserController.class).deleteUser(user.get().getId())).withRel("delete");
		
		entityModel.add(selfLink);
		entityModel.add(delLink);
		return entityModel;  
	}
	
}
