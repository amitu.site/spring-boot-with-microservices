package com.amitu.tutorial.springboot.userservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amitu.tutorial.springboot.userservice.exception.UserNotFoundException;
import com.amitu.tutorial.springboot.userservice.model.User;
import com.amitu.tutorial.springboot.userservice.repository.UserRepository;

@RestController
public class UserController {
	
	@Autowired
	private UserRepository repository;
	
	@RequestMapping(path = {"greetings", "hello"}, method = {RequestMethod.GET})
	public String greetings() {
		return "Hello World";
	}
	
	@RequestMapping(path = {"user"}, method = {RequestMethod.POST}) 
	public ResponseEntity<User> addUser(@RequestBody User user) {
		User createdUser = repository.save(user);
		return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
	}
	
	@GetMapping("/user/{id}")
	public ResponseEntity<Optional<User> > getUser(@PathVariable("id")Long id) throws UserNotFoundException {
		try {
			Optional<User> user = repository.findById(id);
			return new ResponseEntity<>(user, HttpStatus.OK);
		} catch (Exception e) {
			throw new UserNotFoundException("User Not Found with User Id: "+id);
		}
	}
	
	@PutMapping("/user")
	public ResponseEntity<User> updateUser(@RequestBody User user) {	
		try {
			User updatedUser = repository.save(user);
			return new ResponseEntity<>(updatedUser, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<User>(HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@DeleteMapping("/user/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable("id")Long id) throws UserNotFoundException {
		try {
			repository.deleteById(id);;
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			throw new UserNotFoundException("User Not Found with User Id: "+id);
		}
	}
	
	
}
