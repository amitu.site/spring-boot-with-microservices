package com.amitu.tutorial.springboot.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.amitu.tutorial.springboot.userservice.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
