package com.amitu.tutorial.springboot.userservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amitu.tutorial.springboot.userservice.exception.UserNotFoundException;
import com.amitu.tutorial.springboot.userservice.model.User;
import com.amitu.tutorial.springboot.userservice.repository.UserRepository;

@RestController
@RequestMapping("userconverter")
public class CustomHttpMessageResolverController {

	@Autowired
	private UserRepository repository;
	
	@RequestMapping(path = {"user"}, method = {RequestMethod.POST}, consumes = "text/user") 
	public ResponseEntity<User> addUser(@RequestBody User user) {
		try {
			
			User createdUser = repository.save(user);
			return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
			
		} catch (Exception e) {
			User user1 = new User();
			return new ResponseEntity<User>(user1,HttpStatus.EXPECTATION_FAILED);
		}
		
	}
	
	@RequestMapping(path = {"users/{id}"}, method = {RequestMethod.GET}, produces = "text/user", consumes = "text/user")
	public ResponseEntity<User> getUser(@PathVariable("id")Long id) throws UserNotFoundException {
		try {
			Optional<User> user = repository.findById(id);
			return new ResponseEntity<User>(user.get(), HttpStatus.OK);
		} catch (Exception e) {
			throw new UserNotFoundException("User Not Found with User Id: "+id);
		}
	}
	
}
